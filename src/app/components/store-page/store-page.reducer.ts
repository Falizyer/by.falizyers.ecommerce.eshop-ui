import { UserModel } from '../../core.model/user.model';
import { EquipmentDiller, StoreEquipment } from './store-page.model';
import { StorePageAction, StorePageActionPayload } from './store-page.action';
import { AppAction } from '../../app.action';

export interface IStorePageState {
  user: UserModel;
  equipmentList: StoreEquipment[];
  dillers: EquipmentDiller[];
}

const defaultState: IStorePageState = {
  user: null,
  equipmentList: [],
  dillers: []
};

export const storePageReducer = (state: IStorePageState = defaultState, action: AppAction<StorePageActionPayload>) => {
  switch (action.type) {
    case StorePageAction.LOAD_DATA:
      const {
              user          = state.user,
              equipmentList = state.equipmentList,
              dillers       = state.dillers
            } = action.payload;
      return Object.assign({}, state, {
        user,
        equipmentList,
        dillers
      });
    case StorePageAction.LOAD_NEW_EQUIPMENT:
      const {equipment} = action.payload;
      state.equipmentList.push(equipment);
      return Object.assign({}, state, {
        equipmentList: state.equipmentList
      });
    default:
      return state;
  }
};
