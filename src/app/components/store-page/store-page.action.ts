import { Injectable } from '@angular/core';
import { AppAction } from '../../app.action';
import { EquipmentDiller, StoreEquipment, StorePageModel } from './store-page.model';
import { UserModel } from '../../core.model/user.model';

export interface LoadDataPayload {
  user: UserModel;
  equipmentList: StoreEquipment[];
  dillers: EquipmentDiller[];
}

export interface LoadNewEquipmentPayload {
  equipment: StoreEquipment;
}

export type StorePageActionPayload = LoadDataPayload & LoadNewEquipmentPayload;

@Injectable({
  providedIn: 'root'
})
export class StorePageAction {

  static readonly LOAD_DATA = 'STORE_PAGE_ACTION.LOAD_DATA';
  static readonly LOAD_NEW_EQUIPMENT = 'STORE_PAGE_ACTION.LOAD_NEW_EQUIPMENT';

  loadData(payload: LoadDataPayload): AppAction<LoadDataPayload> {
    return {
      type: StorePageAction.LOAD_DATA,
      payload
    };
  }

  loadNewEquipment(payload: LoadNewEquipmentPayload): AppAction<LoadNewEquipmentPayload> {
    return {
      type: StorePageAction.LOAD_NEW_EQUIPMENT,
      payload
    };
  }
}
