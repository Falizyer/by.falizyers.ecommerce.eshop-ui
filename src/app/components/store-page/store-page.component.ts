import { Component, OnInit } from '@angular/core';
import { StorePageService } from './store-page.service';
import { Observable } from 'rxjs';
import { select$, Transformer } from '@angular-redux/store';
import { map, tap } from 'rxjs/operators';
import { EquipmentDiller, EquipmentType, StoreEquipment } from './store-page.model';
import { IStorePageState } from './store-page.reducer';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';

const equipmentMapper: Transformer<IStorePageState, StoreEquipment[]> =
        obs$ => obs$.pipe(map((state: IStorePageState) => state.equipmentList));

const dillerMapper: Transformer<IStorePageState, EquipmentDiller[]> =
        obs$ => obs$.pipe(map((state: IStorePageState) => state.dillers));

@Component({
  selector: 'app-store-page',
  templateUrl: './store-page.component.html',
  styleUrls: [ './store-page.component.scss' ]
})
export class StorePageComponent implements OnInit {

  @select$('storePage', equipmentMapper)
  equipmentList$: Observable<StoreEquipment[]>;

  @select$('storePage', dillerMapper)
  dillerList$: Observable<EquipmentDiller[]>;

  createForm: FormGroup;

  equipmentType: EquipmentType[] = [ EquipmentType.MOTHER_BOARD, EquipmentType.GRAPHIC_CARD ];

  constructor(private storePageService: StorePageService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.storePageService.loadStorePage({userId: 1}).subscribe();
    this.createForm = this.fb.group({
      name: [ '', Validators.required ],
      code: [ '', Validators.required ],
      description: [ '', Validators.required ],
      type: [ EquipmentType.MOTHER_BOARD, Validators.required ],
      price: [ 0, Validators.required ],
      diller: []
    });
  }

  createNewEquipment() {
    const {type, name, code, description, price, diller} = this.createForm.getRawValue();
    this.storePageService.createEquipment({
      code,
      description,
      name,
      type
    }).subscribe();
  }

}
