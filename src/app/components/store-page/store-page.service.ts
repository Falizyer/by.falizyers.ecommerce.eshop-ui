import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ApolloQueryResult } from 'apollo-client';
import { StoreEquipment, StoreEquipmentDto, StorePageModel } from './store-page.model';
import { NgRedux } from '@angular-redux/store';
import { AppModule } from '../../app.module';
import { StorePageAction } from './store-page.action';
import { IAppState } from '../../core.model/app';
import { FetchResult } from 'apollo-link';

export interface IStorePageVariables {
  userId: number;
}

@Injectable({
  providedIn: AppModule
})
export class StorePageService {

  private QEURY_STORE_PAGE = gql`
  query ($userId: ID!) {
    dillers {
      id
      name
    }
    user(userId: $userId) {
      id
      email
    }
    equipmentList: equipments {
      id
      name
      code
      type
      description
      avgPrice
    }
  }
  `;

  private MUTATION_CREATE_EQUIPMENT = gql`
  mutation ($dto: EquipmentDto!) {
    createEquipment(dto: $dto) {
      id
      name
      code
      type
      description
      avgPrice
    }
  }
  `;

  constructor(private apollo: Apollo,
              private ngRedux: NgRedux<IAppState>,
              private storePageAction: StorePageAction) {
  }

  loadStorePage(variables: IStorePageVariables): Observable<ApolloQueryResult<StorePageModel>> {
    return this.apollo.query<StorePageModel>({
      query: this.QEURY_STORE_PAGE,
      variables: {
        ...variables
      }
    }).pipe(
      tap(result => {
        this.ngRedux.dispatch(this.storePageAction.loadData(result.data));
      }),
      catchError(error => {
        throw error;
      })
    );
  }

  createEquipment(dto: StoreEquipmentDto): Observable<ApolloQueryResult<{ createEquipment: StoreEquipment }>> {
    return this.apollo.mutate<any>({
      mutation: this.MUTATION_CREATE_EQUIPMENT,
      variables: {
        dto
      }
    }).pipe(
      tap(result => {
        this.ngRedux.dispatch(this.storePageAction.loadNewEquipment({equipment: result.data.createEquipment}));
      }),
      catchError(error => {
        throw error;
      })
    );
  }
}
