import { UserModel } from '../../core.model/user.model';

export enum EquipmentType {
  MOTHER_BOARD = 'MOTHER_BOARD',
  GRAPHIC_CARD = 'GRAPHIC_CARD'
}

export interface StoreEquipment {
  id: number;
  name: string;
  code: string;
  description: string;
  avgPrice: number;
  type: EquipmentType;
}

export interface EquipmentDiller {
  id: number;
  name: string;
}

export interface EquipmentPrice {
  price: number;
  diller: EquipmentDiller;
}

export interface StoreEquipmentDto {
  id?: number;
  name: string;
  code: string;
  description: string;
  type: EquipmentType;
}

export interface StorePageModel {
  user: UserModel;
  equipmentList: StoreEquipment[];
  dillers: EquipmentDiller[];
}
