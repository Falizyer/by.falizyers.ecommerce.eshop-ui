import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';

import { StorePageRoutingModule } from './store-page-routing.module';
import { StorePageComponent } from './store-page.component';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatSelectModule } from '@angular/material';
import { FlexModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ StorePageComponent ],
  imports: [
    CommonModule,
    StorePageRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FlexModule,
    MatSelectModule,
    ReactiveFormsModule
  ]
})
export class StorePageModule {
}
