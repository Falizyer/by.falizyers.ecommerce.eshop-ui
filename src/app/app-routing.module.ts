import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'store',
    loadChildren: () => import('./components/store-page/store-page.module')
      .then(mod => mod.StorePageModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./components/cart-page/cart-page.module')
      .then(mod => mod.CartPageModule)
  },
  {
    path: '',
    redirectTo: 'store',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
