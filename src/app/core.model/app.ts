import { IStorePageState } from '../components/store-page/store-page.reducer';

export interface IAppState {
  route: string;
  storePage: IStorePageState;
}
