import { Component } from '@angular/core';
import { DevToolsExtension, NgRedux } from '@angular-redux/store';
import { NgReduxRouter, routerReducer } from '@angular-redux/router';
import { combineReducers } from 'redux';
import { storePageReducer } from './components/store-page/store-page.reducer';
import { IAppState } from './core.model/app';
import { OverlayContainer } from '@angular/cdk/overlay';

const defaultState: IAppState = {
  route: '',
  storePage: void 0
};

const reducers = combineReducers<IAppState>({
  route: routerReducer,
  storePage: storePageReducer
});

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent {
  title = 'eshop-ui';

  public constructor(overlayContainer: OverlayContainer,
                     ngRedux: NgRedux<IAppState>,
                     devTools: DevToolsExtension,
                     ngReduxRouter: NgReduxRouter) {
    overlayContainer.getContainerElement().classList.add('unicorn-dark-theme');
    const enhancers = devTools.isEnabled() ? [ devTools.enhancer() ] : [];
    ngRedux.configureStore(reducers, defaultState, [], enhancers);
    ngReduxRouter.initialize();
  }
}
